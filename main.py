
#je crée une fonction qui ajoute la lettre recherchée dans une liste que j'ai appelé "position_lettre"
def position_lettre(phrase, caractere):
    liste_positions = []
    compteur = 0
    for  lettre in phrase:
        compteur +=1
        if lettre == caractere:
            liste_positions.append(compteur)
    return liste_positions


#demande à l'utilisateur de la phrase à vérifier
phrase = input("Veuillez saisir une phrase : ")

#demande à l'utilisateur de la lettre à rechercher
lettre_recherche = input("Veuillez saisir la lettre à rechercher : ")

#si la lettre recherchée est égale à une lettre de la phrase alors je l'ajoute à une liste position_lettre
if lettre_recherche in phrase:
    # je créé une nouvelle valeur (positions) qui est égale à la fonction position_lettre créée plus tôt auquel j'attribue les
    #valeurs saisies par l'utilisateur (phrase et lettre_recherche)
    positions = position_lettre(phrase, lettre_recherche)

    for position in positions:
        print("on retrouve le caractère ", lettre_recherche, " à la position ", position)
else:
    print("La lettre n'est pas dans la phrase")

